Predicting expert users in community Q&A sites
=================================================
CSC 722: Advanced Machine Learning 
------------------------------------------------

Abstract
------------------------------------------------
Community Question Answering (CQA) sites like Stack Overflow have been become a 
repository of community knowledge and have been gaining popularity in the past 
few years. Typically the success of such sites depends mainly on the 
contribution of a small number of expert users who provide majority of the 
helpful answers. These users contribute actively and are reliable sources of 
high quality posts. Existing point based reward systems on these Q&A sites mask 
the mastery of a topic by these users. Identifying users that have the 
potential to become strong contributors and credible sources of posts early 
allows the owners of the community to engage these users to encourage them to 
contribute more to the community. This can include directing unanswered 
questions to the expert users and rewarding them via special privileges in the 
community. The goal of this project is to investigate the activity and behavior 
of users in a particular Q&A community and discover whether or not it is 
possible to predict early on in a users career whether or not they will be an 
elite user in the future.
